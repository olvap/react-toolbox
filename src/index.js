import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import theme from './toolbox/theme'
import ThemeProvider from 'react-toolbox/lib/ThemeProvider'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux'
import './index.css'
import './toolbox/theme.css'
import { getItems } from './actions/'

import store from './store.js'

store.dispatch(getItems())

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>
  , document.getElementById('root'))

registerServiceWorker();

window.store = store
