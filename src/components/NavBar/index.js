import AppBar from 'react-toolbox/lib/app_bar/AppBar'
import Navigation from 'react-toolbox/lib/navigation/Navigation'
import Link from 'react-toolbox/lib/link/Link'
import React from 'react'

const NavBar = ({getItems}) => (
  <AppBar title='Lista de compras'>
    <Navigation type='horizontal'>
      <Link onClick={getItems} icon='autorenew' />
    </Navigation>
  </AppBar>
)

export default NavBar
