import React from 'react'
import { List, ListCheckbox  } from 'react-toolbox/lib/list';

const ShoppingList = ({items, toggleItem}) => (
  <List selectable ripple>
  {items.map(({name, id, done}, index) => (

    <ListCheckbox
      caption={name}
      key={id}
      onChange={() => toggleItem(id)}
      checked={done}
    />
  ))}
  </List>
)

export default ShoppingList
