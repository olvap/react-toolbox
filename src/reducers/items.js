const toggleItem = (items, index) => (
  [
    ...items.slice(0, index),
    Object.assign(items[index], { done: !items[index].done }),
    ...items.slice(index + 1)
  ]
)

export default (items = [], { type, payload, id }) => {
  switch(type) {
    case 'SET_ITEMS':
      return payload

    case 'TOGGLE_ITEM':
      return toggleItem(items, items.findIndex(item => item.id === id))

    default:
      return items
  }
}
