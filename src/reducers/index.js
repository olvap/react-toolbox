import { combineReducers } from 'redux'
import items from './items.js'

export default combineReducers({ items })
