import reducer from '../index.js'

describe("articles reducer", () => {

  const initialState = {}

  describe('SET_ITEMS', () => {
    it('should set the collection', () => {
      const collection = [{ name: 'new data' }, {name: 'other'}]
      const state = reducer(
        initialState,
        { type: 'SET_ITEMS', payload: collection }
      )

      expect(state.items).toEqual(collection)
    })
  })

  describe('TOGGLE_ITEM', () => {
    it('set done to false when is true', () => {
      const initialState = {
        items: [{name: 'arreglar heladera', id: 1}]
      }
      const state = reducer(
        initialState,
        { type: 'TOGGLE_ITEM', id: 1}
      )

      expect(state.items[0].done).toEqual(true)
    })

    it('set done to true when is false', () => {
      const initialState = {
        items: [{name: 'arreglar heladera', id: 1, done: true}]
      }
      const state = reducer(
        initialState,
        { type: 'TOGGLE_ITEM', id: 1}
      )

      expect(state.items[0].done).toEqual(false)
    })
  })
})
