export const getItems = () => (
  {
    type: 'API',
    url: 'items',
    method: 'GET',
    success: 'SET_ITEMS',
    // before_fetching: 'FETCH_ITEMS'
  }
)

export const toggleItem = (id) => (
  {
    type: 'TOGGLE_ITEM',
    id: id
  }
)
