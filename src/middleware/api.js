const BASE_URL = 'http://localhost:3000/'

const apiMiddleware = ({ dispatch }) => next => action => {
  if (action.type !== 'API') {
    return next(action)
  }

  const url = BASE_URL + action.url

  if(action.before_fetching){
    dispatch({type: action.before_fetching})
  }

  fetch(
    url,
    {
      method: action.method,
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(action.body)
    }
  )
    .then(response => response.json())
    .then((payload) => {
      dispatch({ type: action.success, payload, id: action.id })
    })
}

export default apiMiddleware;
