import React from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getItems, toggleItem  } from './actions/'
import NavBar from './components/NavBar/'
import ShoppingList from './components/ShoppingList/'

const App = ({items, getItems, toggleItem}) =>
  <div className="App">
    <NavBar getItems={getItems}/>
    <ShoppingList items={items} toggleItem={toggleItem}/>
  </div>

App.propTypes = {
  items: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  items: state.items
})

const mapDispatchToProps = {
  getItems: getItems,
  toggleItem: toggleItem
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
