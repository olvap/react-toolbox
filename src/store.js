import { createStore, applyMiddleware } from 'redux'
import rootReducers from './reducers/index.js'
import apiMiddleware from './middleware/api.js'

export default createStore(
  rootReducers,
  applyMiddleware(apiMiddleware)
)
